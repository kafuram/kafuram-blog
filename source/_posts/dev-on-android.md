---
title: Android上の開発環境を整備した
date: 2019-12-31 00:43:54
categories:
- Diary
tags:
- Diary
- Development
- Android
---
現在使用可能な開発環境がAndroid上のTermuxのみなので、出来る限り良質なものとなるようにカスタムしました。

以下、行ったことを記録します。
<!-- more -->
## バイブの無効化

行頭でBackSpaceを入力した際に発生するバイブを無効化します。

{% codeblock %}
echo bell-character=ignore > ~/.termux/termux.properties
{% endcodeblock %}

その後アプリを再起動すると、バイブが鳴らなくなります。

## フォントの変更

好きなフォントを使うことでモチベを保ちます。

ここではRicty Diminished Discord Boldを導入します。

{% codeblock %}
pkg install wget
wget https://github.com/edihbrandon/RictyDiminished/raw/master/RictyDiminishedDiscord-Bold.ttf -O ~/.termux/font.ttf
{% endcodeblock %}

その後アプリを再起動すると、フォントが変わります。

## シェルの変更

fishが流行っているので。

{% codeblock %}
pkg install fish
chsh -s fish
{% endcodeblock %}

その後アプリを再起動すると、シェルが変わります。

## pureのインストール

zshテーマのpureのfish版です。

シンプルなので愛用しています。

{% codeblock %}
curl https://git.io/fisher --create-dirs -sLo ~/.config/fish/functions/fisher.fish
fisher add rafaelrinaldi/pure
{% endcodeblock %}

## NeoVimのインストール

好みに別れますよね。

{% codeblock %}
pkg install neovim
{% endcodeblock %}

## Draculaのインストール

Visual Studio Codeで愛用しているテーマです。

{% codeblock %}
curl https://raw.githubusercontent.com/Shougo/dein.vim/master/bin/installer.sh > installer.sh
sh ./installer.sh ~/.cache/dein
{% endcodeblock %}

ここで出力されるVimScriptを `~/.config/nvim/init.vim` に書き込みます。

{% codeblock lang:vim ~/.config/nvim/init.vim %}
" 任意の位置に挿入
call dein#add('dracula/vim')

" dein.vimの範囲外に挿入
colorscheme dracula
{% endcodeblock %}

{% codeblock lang:vim %}
:call dein#install()
{% endcodeblock %}

## NeoVimの設定

行数とカレント行ハイライトを付けます。

{% codeblock lang:vim ~/.config/nvin/init.vim %}
" dein.vimの範囲外に挿入
set cursorline
hi cursorline cterm=NONE ctermbg=237 ctermfg=NONE guibg=darkred guifg=white
{% endcodeblock %}

## 結果

{% asset_img shell.jpg %}
{% asset_img neovim.jpg %}

こうなります。
